defmodule GrainTest do

  use ExUnit.Case, async: true

  doctest GrainsEngine.Grain
  alias GrainsEngine.Grain

  describe "populated Grain" do

    setup do
        {:ok, grain} = Grain.start_link(["foo", "bar"])
        {:ok, grain: grain}
    end

    test "represents string", %{grain: grain} do
        assert Grain.to_string(grain) == "{bar, foo}"
    end

    test "retrieve parameter", %{grain: grain} do
      assert Grain.get_value(grain, :foo) == :none
    end


    test "set parameter", %{grain: grain} do
      assert Grain.get_value(grain, :foo) == :none
      Grain.set_value(grain, :foo, 63)
      assert Grain.get_value(grain, :foo) == 63
    end

    test "get all parameters", %{grain: grain} do
      Grain.set_value(grain, :foo, 1)
      Grain.set_value(grain, :bar, 2)
      assert Grain.get_params(grain) == %{bar: 2, foo: 1}
    end


  end

  describe "empty Grain" do

    setup do
        {:ok, grain} = Grain.start_link([])
        {:ok, grain: grain}
    end

    test "represents string", %{grain: grain} do
        assert Grain.to_string(grain) == "{}"
    end
  end

end
