defmodule OscSenderTest do

  use ExUnit.Case, async: true

  doctest GrainsEngine.OscSender
  alias GrainsEngine.{OscSender, Player}


  describe "convert values to osc values" do

    test "with floats" do
      assert OscSender.build_osc_value(0.5) == {:osc_float, 0.5}
    end

    test "with integers" do
      assert OscSender.build_osc_value(5) == {:osc_float, 5.0}
    end

  end


  describe "convert a raw tuple" do
    assert OscSender.build_osc_tuple([0.1, 0.2, 3.0]) == [osc_float: 0.1,
                                                        osc_float: 0.2,
                                                        osc_float: 3.0]
  end

  describe "convert a map" do
    assert OscSender.build_osc_tuple( %{"pitch" => 73, "roll" => 74, "yaw" => 3} ) == [osc_float: 73.0,
                                                                                       osc_float: 74.0,
                                                                                       osc_float: 3.0]
  end


end
