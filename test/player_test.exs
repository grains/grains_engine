defmodule PlayerTest do

  use ExUnit.Case, async: true

  doctest GrainsEngine.Player
  alias GrainsEngine.Player

  describe "populated Player" do

    setup do
        {:ok, player} = Player.start_link("frank", ["foo", "bar"])
        {:ok, player: player}
    end

    test "represents string", %{player: player} do
        assert Player.to_string(player) == "%Player{:name  => frank,\n:grain => {bar, foo}}"
    end

    test "set all values for the player", %{player: player} do
      values = %{foo: 1, bar: 2}
      Player.update_parameters(player, values)
      assert Player.get_values(player) == %{bar: 2, foo: 1}
    end


  end

end
