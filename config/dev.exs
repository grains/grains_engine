use Mix.Config

config :grains_engine,
  osc_client_ip: {192, 168, 1, 58},
  osc_client_port: 47122,
  phoenix_url: "ws://localhost:4000/socket/websocket?token=foobar"
