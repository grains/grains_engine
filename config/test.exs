use Mix.Config

config :grains_engine,
  osc_client_ip: {127,0,0,1},
  osc_client_port: 8001,
  phoenix_url: "ws://localhost:4000/socket/websocket?token=foobar"
