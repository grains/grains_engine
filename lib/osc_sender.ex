defmodule GrainsEngine.OscSender do


  alias GrainsEngine.{OscSender, Player, Grain}
  @doc """
  starts a new sending process for OSC on `ip` (tuple) and `port`
  """
  def start_link(ip, port) do
    Agent.start_link(fn -> {ip, port} end, name: __MODULE__)
  end

  @doc """
  Sends an OSC `message` with `value` on a given `channel`
  """

  def send(channel, message) do
    Agent.get(__MODULE__, fn {ip, port} ->
      IO.puts "sending to #{channel} #{inspect(message)}"
      ExOsc.Sender.send_message(ip, port, {channel, message})
      {ip, port}
    end)
  end

  def send_grain(name, grain) do
    Grain.get_params(grain)
    |> Enum.each(fn {key, value} ->
      send_osc_value(name, key, value)
    end)
  end


  def send_osc_value(name, key, value) do
      channel = "/#{name}/#{key}"
      OscSender.send_raw(channel, value)
  end


  def send_raw(channel, value) when is_integer(value) do
    OscSender.send(channel, {:osc_float, value / 1})
  end

  def send_raw(channel, value) when is_float(value) do
    OscSender.send(channel, {:osc_float, value})
  end

  def send_raw(channel, value) when is_tuple(value) or is_map(value) do
    OscSender.send(channel, build_osc_tuple(value))
  end

  def send_raw(_channel, _value) do
  end


  def build_osc_tuple(values) do
    values
    |> Enum.map(fn v ->
      build_osc_value(v)
    end)
  end

  # def build_osc_tuple(values) when is_map(values) do
  #   values
  #   |> Enum.map(fn v ->
  #     build_osc_value(v)
  #   end)
  # end

  def build_osc_value(v) when is_integer(v) do
    {:osc_float, v / 1}
  end

  def build_osc_value(v) when is_float(v) do
    {:osc_float, v}
  end

  def build_osc_value(map = {_k, v}) when is_tuple(map) do
    build_osc_value(v)
  end




  def send_player(player) do
    name = Player.name(player)
    Player.get_values(player)
    |> Enum.each(fn {k,v} ->
      channel = "/#{name}/#{k}"
      OscSender.send(channel, {:osc_float, v})
    end)


  end



end
