defmodule GrainsEngine.Player do
  alias GrainsEngine.{Player, Grain, Parameter}

  defstruct name: :none, grain: :none

  @doc """
  Starts a new player with a `name` and a list of names of `params`
  """
  def start_link(name, params) do
    {:ok, grain} = Grain.start_link(params)
    Agent.start_link(fn -> %Player{name: name,
                                   grain: grain} end)
  end


  @doc """
  Updates the values of the parameters. Takes a `params` map that
  has the same keys as the ones specified when the `player` was
  created.
  """
  def update_parameters(player, params) do
    grain = Player.grain(player)
    Enum.each(params, fn {k, v} ->
      Grain.set_value(grain, k, v)
    end)

  end

  @doc """
  Returns a Map in the form %{foo: 1, bar: 2} with the
  values and the parameters.
  """
  def get_values(player) do
    grain = Player.grain(player)
    Grain.get_params(grain)
  end


  def set_name(player, name) do
    Agent.update(player, fn state -> Map.put(state, :name, name) end)
  end

  def name(player) do
    Agent.get(player, fn state -> state.name end)
  end


  def set_grain(player, grain) do
    Agent.update(player, fn state -> Map.put(state, :grain, grain) end)
  end

  def grain(player) do
    Agent.get(player, fn state -> state.grain end)
  end




  def to_string(player) do
    "%Player{" <> string_body(player) <> "}"
  end

  def string_body(player) do
    state = Agent.get(player, &(&1))
    ":name  => " <> name_to_string(state.name) <> ",\n" <>
      ":grain => " <> Grain.to_string(state.grain)
  end

  defp name_to_string(:none) , do: "none"
  defp name_to_string(name), do: name



end
