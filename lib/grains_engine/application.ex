defmodule GrainsEngine.Application do
  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  require Logger

  def start(_type, args) do
    # import Supervisor.Spec, warn: false

    # ip = Application.fetch_env!(:grains_engine, :osc_client_ip)
    # port = Application.fetch_env!(:grains_engine, :osc_client_port)
    # Logger.info "Supervising #{port}"

    # # Define workers and child supervisors to be supervised
    # children = [
    #   # Starts a worker by calling: GrainsEngine.Worker.start_link(arg1, arg2, arg3)
    #   # worker(GrainsEngine.Worker, [arg1, arg2, arg3]),
    #   worker(GrainsEngine.OscSender, [ip, port]),
    #   supervisor(GrainsEngine.PerformanceSupervisor, [])
    # ]

    # Logger.warn "Starting stuff"
    # # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # # for other strategies and supported options
    # opts = [strategy: :one_for_one, name: GrainsEngine.PerformanceSupervisor]
    # Supervisor.start_link(children, opts)
  end
end
2
