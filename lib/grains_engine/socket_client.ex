defmodule GrainsEngine.SocketClient do
  @moduledoc false
  require Logger
  alias Phoenix.Channels.GenSocketClient
  alias GrainsEngine.{Performance, PerformanceSupervisor }

  @behaviour GenSocketClient

  # "ws://localhost:4000/socket/websocket?token=foobar"
  def start_link() do
    phoenix_url = Application.fetch_env!(:grains_engine, :phoenix_url)
    Logger.info "SocketClient.start_link"
    GenSocketClient.start_link(
      __MODULE__,
      Phoenix.Channels.GenSocketClient.Transport.WebSocketClient,
      phoenix_url
    )
  end

  def init(url) do
    Logger.info "SocketClient.init #{url}"

    {:connect, url, %{}}
  end

  def handle_connected(transport, state) do
    Logger.info("connected, joining")
    GenSocketClient.join(transport, "osc")
    {:ok, state}
  end

  def handle_disconnected(reason, state) do
    Logger.error("disconnected: #{inspect reason}")
    Process.send_after(self(), :connect, :timer.seconds(1))
    {:ok, state}
  end

  def handle_joined(topic, _payload, _transport, state) do
    Logger.info("joined the topic #{topic}")
    {:ok, state}
    # if state.first_join do
    #   :timer.send_interval(:timer.seconds(10), self(), :ping_server)
    #   {:ok, %{state | first_join: false, ping_ref: 1}}
    # else
    #   {:ok, %{state | ping_ref: 1}}
    # end
  end

  def handle_join_error(topic, payload, _transport, state) do
    Logger.error("join error on the topic #{topic}: #{inspect payload}")
    {:ok, state}
  end

  def handle_channel_closed(topic, payload, _transport, state) do
    Logger.error("disconnected from the topic #{topic}: #{inspect payload}")
    Process.send_after(self(), {:join, topic}, :timer.seconds(1))
    {:ok, state}
  end

  def handle_message("osc", "presence", payload, _transport, state) do
    user = payload["user"]
    Logger.info("Starting Performance for #{user}")
    PerformanceSupervisor.start_performance(user)

    {:ok, state}
  end

  def handle_message("osc", "performance", payload, _transport, state) do
    Logger.info "Performance event received"
    Logger.info inspect(payload)

    {:ok, state}

  end

  def handle_message("osc", "clock", _payload, _transport, state) do
    channel = "/clock"
    message = 1
    GrainsEngine.OscSender.send_raw(channel, message)
    message = 0
    GrainsEngine.OscSender.send_raw(channel, message)

    {:ok, state}
  end

  def handle_message("osc", "slider", payload, _transport, state) do
    Logger.warn("Slider: #{inspect payload}")

    # Enum.each(payload, fn({ user, values} ) ->
    #   Enum.each(values, fn({param, value}) ->
    #     GrainsEngine.OscSender.send_osc_value(user, param, value)
    #   end)

    # end)

    Enum.each(payload, fn({user, value}) ->
      GrainsEngine.OscSender.send_raw("/webosc/gyro", value)
    end)

    {:ok, state}

  end
  def handle_message(topic, event, payload, _transport, state) do
    Logger.warn("message on topic #{topic}: #{inspect event} #{inspect payload}")
    {:ok, state}
  end



  def handle_reply("ping", _ref, %{"status" => "ok"} = payload, _transport, state) do
    Logger.info("server pong ##{payload["response"]["ping_ref"]}")
    {:ok, state}
  end
  def handle_reply(topic, _ref, payload, _transport, state) do
    Logger.warn("reply on topic #{topic}: #{inspect payload}")
    {:ok, state}
  end

  def handle_info(:connect, _transport, state) do
    Logger.info("connecting")
    {:connect, state}
  end
  def handle_info({:join, topic}, transport, state) do
    Logger.info("joining the topic #{topic}")
    case GenSocketClient.join(transport, topic) do
      {:error, reason} ->
        Logger.error("error joining the topic #{topic}: #{inspect reason}")
        Process.send_after(self(), {:join, topic}, :timer.seconds(1))
      {:ok, _ref} -> :ok
    end

    {:ok, state}
  end
  def handle_info(:ping_server, transport, state) do
    Logger.info("sending ping ##{state.ping_ref}")
    GenSocketClient.push(transport, "ping", "ping", %{ping_ref: state.ping_ref})
    {:ok, %{state | ping_ref: state.ping_ref + 1}}
  end
  def handle_info(message, _transport, state) do
    Logger.warn("Unhandled message #{inspect message}")
    {:ok, state}
  end
end
