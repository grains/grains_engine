defmodule GrainsEngine.PerformanceSupervisor do
  use Supervisor

  require Logger

  def start_link do

    Logger.info "Starting PerformanceSupervisor"
    Supervisor.start_link(__MODULE__, [], name: :grains_supervisor)
  end

  def start_performance(name) do
    Supervisor.start_child(:grains_supervisor, [name])
  end

  def init(_) do
    children = [
      worker(GrainsEngine.Performance, [])
    ]

    supervise(children, strategy: :simple_one_for_one)
  end
end
