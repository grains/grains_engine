defmodule GrainsEngine.Performance do
  use GenServer


  alias GrainsEngine.Performance
  alias GrainsEngine.Grain

  def start_link(name) do
    GenServer.start_link(__MODULE__, [], name: via_tuple(name))
  end

  defp via_tuple(performance_name) do
    {:via, :gproc, {:n, :l, {:performance, performance_name}}}
  end

  def set_values(performance_name, params) do
    GenServer.cast(via_tuple(performance_name), {:set_values, params})
  end

  def get_values(performance_name) do
    GenServer.call(via_tuple(performance_name), :get_values)
  end

  def send_values(performance_name) do
    GenServer.cast(via_tuple(performance_name), {:send_values, performance_name})
  end

  # SERVER

  def init(params) do
    {:ok, grain} = Grain.start_link(["s1", "s2"])
    {:ok, grain}
  end

  def handle_cast({:set_values, params}, grain) do
    Enum.each(params, fn {k, v} ->
      Grain.set_value(grain, k, v)
    end)
    {:noreply, grain}
  end

  def handle_cast({:send_values, name}, grain) do
    IO.puts("sending: #{name} #{inspect grain}")
    GrainsEngine.OscSender.send_grain(name, grain)
    {:noreply, grain}
  end

  def handle_call(:get_values, _from, grain) do
    {:reply, Grain.get_params(grain), grain}
  end

  ###




end
