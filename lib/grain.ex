defmodule GrainsEngine.Grain do

  alias GrainsEngine.Parameter

  def start_link(param_names) when is_list param_names do
    Agent.start_link(fn -> initialize_paramters(param_names) end)
  end

  defp initialize_paramters(names) do
    Enum.reduce(names, %{}, fn(name, grain) ->
      key = String.to_atom(name)
      {:ok, parameter } = Parameter.start_link(key)
      Map.put_new(grain, key, parameter)
    end)
  end

  def get_value(grain, key) when is_atom key do
    parameter = Agent.get(grain, fn grain -> grain[key] end)
    Parameter.value(parameter)
  end

  def set_value(grain, key, value) when is_atom key do
    parameter = Agent.get(grain, fn grain -> grain[key] end)
    Parameter.set_value(parameter, value)
  end

  def get_params(grain) do
    Agent.get(grain, fn state -> state end)
    |> Enum.reduce(%{}, fn{k,v}, acc ->
      value = Parameter.value(v)
      Map.put(acc, k, value)
    end)
  end

  def to_string(grain) do
    grain_string =
      Agent.get(grain, fn state -> state end)
      |> Enum.reduce("", fn {k, _v}, acc -> "#{acc}, #{k}" end )

    "{" <> String.replace_leading(grain_string, ", ", "") <> "}"
  end

end
