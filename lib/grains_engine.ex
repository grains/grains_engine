defmodule GrainsEngine do

  use Application
  require Logger

  @moduledoc """
  Documentation for GrainsEngine.
  """


  def start(_type, _args) do
    Logger.info "Application starting"
    import Supervisor.Spec, warn: false


    ip = Application.fetch_env!(:grains_engine, :osc_client_ip)
    port = Application.fetch_env!(:grains_engine, :osc_client_port)
    Logger.info "Supervising #{port}"

    children = [
      worker(GrainsEngine.OscSender, [ip, port]),
      supervisor(GrainsEngine.PerformanceSupervisor, []),
      worker(GrainsEngine.SocketClient, [])
    ]

    opts = [strategy: :one_for_one, name: GrainsEngine.Supervisor]
    Supervisor.start_link(children, opts)
  end


end
