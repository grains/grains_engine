defmodule GrainsEngine.Parameter do

  defstruct name: :none, value: :none

  alias GrainsEngine.Parameter


  def start_link(name \\ :none) do
    Agent.start_link(fn -> %Parameter{name: name, value: :none} end)
  end


  def name(parameter) do
    Agent.get(parameter, fn state -> state.name end)
  end

  def value(parameter) do
    Agent.get(parameter, fn state -> state.value end)
  end

  def set?(parameter) do
    case Agent.get(parameter, fn state -> state.name end) do
      :none -> false
      _ -> true
    end
  end

  def set_value(parameter, value) when is_number value do
    Agent.update(parameter, fn state -> Map.put(state, :value, value) end)
  end

  def set_name(parameter, name) do
    Agent.update(parameter, fn state -> Map.put(state, :name, name) end)
  end


  def to_string(parameter) do
    "(name:#{name(parameter)}: #{value(parameter)})"
  end

end
